package cz.uhk.pro2.flappy.gui;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.Timer;

import cz.uhk.pro2.flappy.game.GameBoard;

public class MainWindow extends JFrame {
	GameBoard gameBoard;
	BoardPanel pnl = new BoardPanel();
	long x = 0;
	
	
	class BoardPanel extends JPanel {
		@Override
		public void paint(Graphics g) {
			super.paint(g);
			gameBoard.draw(g);
		}
	}
	
	public MainWindow(){
		gameBoard = new GameBoard();
		add(pnl,BorderLayout.CENTER);
		pnl.setPreferredSize(new Dimension(200, 200));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		
		
		addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e){
				gameBoard.kickTheBird();
			} 
		});
		
		Timer t = new Timer(20, e -> {
			gameBoard.tick(x++);
			pnl.repaint();
		});
		
		t.start();
	}
	
	
	public static void main(String[] args) {
		SwingUtilities.invokeLater(()-> {
			MainWindow w = new MainWindow();
			w.setVisible(true);
		});
	}

}
