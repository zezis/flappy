package cz.uhk.pro2.flappy.game;

import java.awt.Color;
import java.awt.Graphics;

public class Bird implements TickAware {
	//fyzika
	static final double koefUp = -5.0;
	static final double koefDown = 2.0;
	static final int ticksFlyingUp = 4;
	
	int viewPortX, viewPortY;
	
	double velocityY = koefDown;
	int tickToFall = 0;
	
	
	public Bird(int initialX, int initialY) {
		this.viewPortX = initialX;
		this.viewPortY = initialY;
	}
	
	@Override
	public void tick(long tickSinceStart) {
		viewPortY += velocityY;		
		if (tickToFall > 0) {
			tickToFall--;
		} else {
			velocityY = koefDown;
		}
	}
	
	public void kick(){
		velocityY = koefUp;
		tickToFall = ticksFlyingUp;
	}
	
	
	public void draw(Graphics g){
		g.setColor(Color.GREEN);
		g.fillOval(viewPortX - Tile.SIZE/2, viewPortY-Tile.SIZE/2, Tile.SIZE, Tile.SIZE);
		g.setColor(Color.BLACK);
		g.drawString(viewPortX + ", "+ viewPortY, viewPortX, viewPortY);
	}
}
