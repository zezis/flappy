package cz.uhk.pro2.flappy.game.tiles;

import java.awt.Graphics;
import java.awt.Image;

import cz.uhk.pro2.flappy.game.Tile;

public class WallTile implements Tile {
	
	Image image;
	
	public WallTile() {
		
	}
	
	public WallTile(Image img){
		image = img;
	}
	
	@Override
	public void draw(Graphics g, int x, int y) {
		g.drawImage(image,x,y,null);
	}
}
