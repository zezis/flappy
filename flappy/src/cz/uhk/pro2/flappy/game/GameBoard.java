package cz.uhk.pro2.flappy.game;

import java.awt.Graphics;


public class GameBoard implements TickAware{
	Tile[][] tiles;
	int shiftX = 30;
	int viewportWidth = 200;
	int widthPix; // sirka hraci plochy v pixelech
	
	Bird bird;
	
	public GameBoard() {
		tiles = new Tile[20][20];
		bird = new Bird(100,100);
	}
	
	public GameBoard(Tile[][] tiles) {
		this.tiles = tiles;
		bird = new Bird(100,100);
	}
	
	
	public void setWidthPix(int widthPix) {
		this.widthPix = widthPix;
	}
	
	@Override
	public void tick(long ticksSinceStart){
		//s kaydym tickem ve hre posuneme hru o jeden pixel
		//tj. pocet ticku a pixelu posunu se rovnaji
		shiftX = (int)ticksSinceStart;
		
		// dame vedet ptakovi, ze hodiny tickly
		bird.tick(ticksSinceStart);
	}
	
	
	public void draw(Graphics g) {
		// j-souradnice prvni dlazdice vlevo, kterou je nutne kreslit
		int minJ = shiftX/Tile.SIZE;
		// pocet dlazdic (na sirku), kolik je nutne kreslit (do viewportu)
		// + 2 protoze muze chybet cast bunky vlevo a vpravo
		// kvuli obema celeciselnum delenim 
		int countJ = widthPix/Tile.SIZE + 2;
		for (int i = 0; i < tiles.length; i++) {			
			for (int j = minJ; j < minJ+countJ; j++) {
				// chceme, aby level bezel porad dokola, takze modJ se
				// na konci pole vraci zase na 0; tiles[0].length je pocet sloupcu
				int modJ = j % tiles[0].length;
				Tile t = tiles[i][modJ];
				
				if (t != null) {
					// v bunce je nejaka dlazdice
					// vykreslime ji
					int viewportX = j*Tile.SIZE - shiftX;
					int viewportY = i*Tile.SIZE;
					t.draw(g, viewportX, viewportY);
				}
			}
		}
		// vykreslit ptaka
		bird.draw(g);
	}
	
	public void kickTheBird() {
		bird.kick();
	}

}
