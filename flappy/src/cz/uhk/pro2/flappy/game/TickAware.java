package cz.uhk.pro2.flappy.game;

/**
 * Rozhrani pro objekty, ktere potrebuji vedet,
 * kolik casu (ticku) ubehlo od zacatku hry
 * @author zezulma1
 *
 */
public interface TickAware {
	
	/**
	 * zmeni stav herni entity s ohledem na zm�nu hern�ho �asu 
	 * @param tickSinceStart cas (pocet ticku) od zahajeni hry
	 */
	void tick(long tickSinceStart);
}
