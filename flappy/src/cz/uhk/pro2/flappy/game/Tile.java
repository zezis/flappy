package cz.uhk.pro2.flappy.game;

import java.awt.Graphics;

public interface Tile {
	public static final int SIZE = 20;
	
	void draw(Graphics g, int x, int y);
}
