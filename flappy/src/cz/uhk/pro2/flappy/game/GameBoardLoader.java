package cz.uhk.pro2.flappy.game;


public interface GameBoardLoader {
	GameBoard loadLevel();
}
